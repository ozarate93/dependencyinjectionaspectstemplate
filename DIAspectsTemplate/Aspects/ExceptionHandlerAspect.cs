﻿using Castle.DynamicProxy;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIAspectsTemplate.Aspects
{
    public class ExceptionHandlerAspect : IInterceptor
    {
       

        public void Intercept(IInvocation invocation)
        {
            try
            {
                invocation.Proceed();
            }
            catch (Exception ex)
            {
                throw ex;
            }
        }
    }
}