﻿using DIAspectsTemplate.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace DIAspectsTemplate.Handlers
{
    public interface IProductHandler
    {
         Product GetById(int id);
    }


    public class ProductHandler : IProductHandler
    {
        public Product GetById(int id)
        {
            var myProduct = new Product() { Name = "Producto 1", Price = 29 };
            return myProduct;
        }
    }
}