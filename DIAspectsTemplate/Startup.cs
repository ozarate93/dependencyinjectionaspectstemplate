﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using Castle.Windsor;
using Castle.Windsor.Installer;

[assembly: OwinStartup(typeof(DIAspectsTemplate.Startup))]

namespace DIAspectsTemplate
{
    public partial class Startup
    {
        public IWindsorContainer _container;
        public void Configuration(IAppBuilder app)
        {
            _container = new WindsorContainer().Install(FromAssembly.This());
            ConfigureAuth(app);
            StartWebApplication(_container);
        }
    }
}
